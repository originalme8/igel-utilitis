#######################################################
# DHCP Option Set (Workaround)
# Version: 1.1.0
#
# Written By: Christopher S. Bates
#
# Note: This script is provided as-is with no support
# from IGEL or the author itself.
#######################################################

## Script Options
REBOOT="true"                                                                                                       # Enable to reboot at the end of the update
ShowMessage="true"                                                                                                  # If set to true, then user will be shown notification of change
HostPattern="ITC"                                                                                                   # Hostname pre-face used for activation
UserMessage="Update Required.\n\nThe structure tag has changed.\n\nThe system will reboot when update is complete." # User Message use "\n" for new line
DHCPFile="/wfs/dhclient*.lease"                                                                                     # DHCP Lease File(s) location "*" can be used for wildcars
DHCPOption="umsstructure"                                                                                           # Searchable string for DHCP option name

## Consistant Variables (Do not Modify without IGEL Support)
IGELReg="system.remotemanager.ums_structure_tag"                                    # IGEL Registry key to compaire / set
StructureTag=$(grep  -m 1 ${DHCPOption} ${DHCPFile} | sed 's/.*"\(.*\)".*/\1/g')    # Get DHCP opti
CurrentReg=$(get ${IGELReg})                                                        # Current Registry setting

if [ ${ShowMessage} = "true" ]; then      # If show messages is set to true, show user message

    if [ -z "${DISPLAY}" ]; then          # If DISPLAY not set, set and export DISPLAY for Zenity messages
        
        export DISPLAY=:0; export DISPLAY
    fi

fi

if [[ $(hostname -s) != ITC* ]]; then

    if [ "${StructureTag}" != "${CurrentReg}" ]; then   # If structure tag has changed, then update and reboot

        if [ ${ShowMessage} = "true" ]; then            # If show messages is set to true, show user message
            
            zenity --notification --text="`printf "${UserMessage}"`"
        
        fi

        setparam ${IGELReg} ${StructureTag} # Set Parameter

        killwait_postsetupd                 # Apply Settings
        write_rmsettings                    # Write settings back to the UMS server (updating structure tag in UMS database)
        
        sleep 5
        get_rmsettings                      #Syncs with UMS server
        killwait_postsetupd                 # Apply Settings after Sync
        sleep 5

        if [ "${REBOOT}" = "true" ]; then
            user_reboot                     # Prompt user for final reboot
        fi

    fi

else

    if [ ${ShowMessage} = "true" ]; then    # If show messages is set to true, show user message

        zenity --notification --text="`printf "Default name detected. Rename to begin configuration."`"
    fi

fi

exit