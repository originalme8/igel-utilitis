# Set UMS Structure Tag



This script provides an automated option for registering an IGEL device that is being managed by an active ICG appliance with a new ICG appliance with a new certificate chain or root certificate.

**Note**: This should happen by default without scripts, but some sites have discovered they have trouble on firmware 11.01. - 11.03.x. This script works as a workaround, and may be removed at a later date and replaced with documentation when issue is identified.

## Prerequisites

- The UMS Structure tag must be setup correctly on the DHCP server
  - Number: 226 
  - Name: umsstructuretag
  - Type: Text
- IGEL OS 11.x (all current versions below 11.04)
- If you are using user messages, then the device hostname must be set, and rebooted before running the script

## Technologies

- bash shell
- IGEL Firmware
- ICG
- UMS
- DHCP Tags

## Installation

1. Configure the variable options in the script to fit your environment (These may be moved to profile variables at a later date)
   - **REBOOT** - prompt for user reboot after update
   - **ShowMessage** - Show user facing messages
   - **HostPattern** - If this is set, the script will not run if the hostname starts with the variable pattern
   - **UserMessage** - Message presented to user when the script is chainging the structure tag
   - **DHCPFile** - Local lease file location (standrd is /wfs/dhclient*.lease)
   - **DHCPOption** - DHCP option name, you must change this if you don't use the default name "umsstructuretag"
2. Create a new profile and label it something that fits your environment
3. Copy the text from StructureTagUpdate.sh into **System** &#8594; **Firmware Customizations** &#8594; **Custom Commands** &#8594; **Network** &#8594; **Final network command**

## Usage

1. Deploy new profile to staging folder
   - **Note:** This can be deployed to other directories, but if devices are renamed without a reboot the mssage function can cause issues
2. Once the device is renamed and rebooted the script should run, and add the structure tag
3. UMS should be updated and apply the policy rules moving the device to the correct location

## Versioning

**Version**: 1.1.0
**Date:** 2020-05-11

We use [SemVer](https://semver.org/) for versioning.