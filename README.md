# IGEL Utilities

In this repository, you will find an assortment of utitilities currently maintained by Christopher S. Bates. They are public, but come with no support or garauntee that they will work.

## Contributing
Please read CONTRIBUTING.md for details on my code of conduct, and the process for submitting pull requests to this repository.

## Versioning

We use [SemVer](https://semver.org/) for versioning.