
#!/bin/bash
##############################################################
# Imprivata Permission Fixer
#
# Original script provided by Imprivata
# Modified by: Christopher S. Bates
#
# Version 1.0.2
# IGEL and Imprivata do not provide official support for this
# This script is a workaround provided for a known issue
##############################################################

ScriptDir="/tmp/scripts"
ScriptName="ImprivataPermissionFix.sh"

if [ ! -f ${ScriptDir}/${ScriptName} ]; then

cat <<'EOF' > "${ScriptDir}/${ScriptName}"
#!/bin/bash
# run this script in


# be sure to detach it !

[ "false" == "$(get imprivata.enabled)" ] && exit 0

DEVCONF="/.imprivata_data/runtime/lib/proveid-embedded/Utils/VendorSpecificDeviceConfigurator.pyc"

while [ ! -f ${DEVCONF} ]; do
  sleep 1
done

logger "IMPRIVATA CUSTOM COMMAND: running ${DEVCONF}"
/usr/lib/imprivata/bin/python ${DEVCONF}

# make sure log files are present and writeable by process with user permission only
touch /.imprivata_data/runtime/log/OneSign.log
touch /.imprivata_data/runtime/log/OneSignAgent.log
chown user:users /.imprivata_data/runtime/log/OneSign*

EOF

chmod +x "${ScriptDir}/${ScriptName}"

fi

"${ScriptDir}/${ScriptName}"

exit