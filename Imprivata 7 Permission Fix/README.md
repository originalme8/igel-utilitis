# Imprivata Python Permissions Fix

There is an identified permissions issue with the combination of software listed below, when using Imprivata Offline Authentication. The workaround is to launch the affected script via a custom command running as the "root" users. The script found on this repository can be used in a profile to execute the pythong script with full permissions, and is a vaiable workaround until Imprivata updates their bootstrap component to fix this issue.

The custom command is only needed with PIE agent version 7.x, it actually runs imprivatas **VendorSpecificDeviceConfigurator** which needs root permission.
As a side effect, it may also create the logfiles, which will then be owned by root which leads to the situation that the PIE Agent itself won't start as it only has user permission. The last 3 lines in the 'ccmd.sh' will prevent this. The need for root permission was re-introduced with version 7.

**Affected Versions:**

- **Imprivata PIE:** 7.x
- **IGEL OS:** 11.03.110

## Prerequisites

- **IGEL OS:** 11.03.110 or later
- **Imprivata Enterprise:** 7.0 or later
- **Imprivata PIE:** 7.0 or later
- Admin access to UMS

## Technologies

- bash shell
- IGEL Firmware
- Imprivata PIE

## Installation

1. Create a new profile and label it something that fits your environment
2. Add a custom command to **System** &#8594; **Firmware Customizations** &#8594; **Custom Commands** &#8594; **Desktop** &#8594; **Final desktop command** 
   - **Note:** If you you are already using final desktop command, you will need to combine the commands into a single script

## Usage

1. Deploy the script file and the profile to all devices running Imprivata PIE 7.0 or later

## Versioning

**Version**: 1.0.1
**Date:** 2020-05-18

We use [SemVer](https://semver.org/) for versioning.